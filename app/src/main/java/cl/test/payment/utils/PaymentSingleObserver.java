package cl.test.payment.utils;

import com.google.gson.Gson;

import java.io.IOException;

import cl.test.payment.data.model.Error;
import io.reactivex.SingleObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import timber.log.Timber;

public abstract class PaymentSingleObserver<T> implements SingleObserver<T> {

    public abstract void onError(Error error);

    @Override
    public void onSuccess(T t) {
        Timber.d("Success!!!");
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            try {
                onError(getErrorMessage(responseBody));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } else {
            Error error = new Error();
            error.setThrowable(e);
            onError(error);
        }
    }

    private Error getErrorMessage(ResponseBody responseBody) throws IOException {
        return new Gson().fromJson(responseBody.string(), Error.class);
    }
}
