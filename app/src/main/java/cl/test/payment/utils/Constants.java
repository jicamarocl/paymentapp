package cl.test.payment.utils;

public class Constants {

    public static String BASE_URL = "https://api.mercadopago.com";
    public static String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";

    public static int REMOTE_CONNECT_TIMEOUT = 1;
    public static int REMOTE_READ_TIMEOUT = 1;
    public static int REMOTE_WRITE_TIMEOUT = 2;

    public static String IS_SHOWING_RESULT = "IS_SHOWING_RESULT";
    public static String SPINNER_SELECTED_POSITION = "SPINNER_SELECTED_POSITION";
}
