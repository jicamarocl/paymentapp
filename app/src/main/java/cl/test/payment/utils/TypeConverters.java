package cl.test.payment.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;

public class TypeConverters {

    @TypeConverter
    public String fromIssuerToString(Issuer issuer) {
        return new Gson().toJson(issuer);
    }

    @TypeConverter
    public Issuer fromStringToIssuer(String issuer) {
        return new Gson().fromJson(issuer, Issuer.class);
    }

    @TypeConverter
    public String fromPaymentMethod(PaymentMethod paymentMethod) {
        return new Gson().toJson(paymentMethod);
    }

    @TypeConverter
    public PaymentMethod fromStringToPaymentMethod(String paymentMethod) {
        return new Gson().fromJson(paymentMethod, PaymentMethod.class);
    }

    @TypeConverter
    public String fromListToString(List<String> strings) {
        return new Gson().toJson(strings);
    }

    @TypeConverter
    public List<String> fromStringToList(String list) {
        return new Gson().fromJson(list, new TypeToken<List<String>>(){}.getType());
    }

    @TypeConverter
    public String fromInstallmentDetailToString(List<InstallmentDetail> installmentDetail) {
        return new Gson().toJson(installmentDetail);
    }

    @TypeConverter
    public List<InstallmentDetail> fromStringToInstallmentDetail(String installmentDetail) {
        return new Gson().fromJson(installmentDetail, new TypeToken<List<InstallmentDetail>>
                (){}.getType());
    }
}
