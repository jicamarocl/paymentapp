package cl.test.payment.di.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import cl.test.payment.data.PaymentRepository;
import cl.test.payment.data.local.LocalDataSource;
import cl.test.payment.data.local.PaymentDataBase;
import cl.test.payment.data.remote.PaymentService;
import cl.test.payment.data.remote.RemoteDataSource;
import cl.test.payment.di.app.PaymentApp;
import cl.test.payment.utils.Constants;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public class GeneralModule {

    private final PaymentApp paymentApp;

    public GeneralModule(PaymentApp paymentApp) {
        this.paymentApp = paymentApp;
    }

    @Provides
    @Singleton
    Context providesApplicationContext() {
        return paymentApp;
    }

    @Provides
    @Singleton
    PaymentDataBase providesDatabase() {
        return PaymentDataBase.buildPersistentDataSource(paymentApp);
    }

    @Provides
    @Singleton
    LocalDataSource providesLocalDataSource(PaymentDataBase paymentDataBase) {
        return new LocalDataSource(paymentDataBase.paymentMethodDao(), paymentDataBase.issuerDao());
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttp() {
        return new OkHttpClient.Builder()
                .connectTimeout(Constants.REMOTE_CONNECT_TIMEOUT, TimeUnit.MINUTES)
                .readTimeout(Constants.REMOTE_READ_TIMEOUT, TimeUnit.MINUTES)
                .writeTimeout(Constants.REMOTE_WRITE_TIMEOUT, TimeUnit.MINUTES)
                .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Timber.d("OkHTTP: %s", message);
                    }
                }))
                .build();
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    PaymentService providesRESTService(Retrofit retrofit) {
        return retrofit.create(PaymentService.class);
    }

    @Provides
    @Singleton
    RemoteDataSource providesRemoteDataSource(PaymentService paymentService) {
        return new RemoteDataSource(paymentService);
    }
}
