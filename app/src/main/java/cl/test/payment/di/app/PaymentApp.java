package cl.test.payment.di.app;

import android.app.Activity;
import android.app.Application;

import java.util.List;

import cl.test.payment.data.model.Installment;
import cl.test.payment.di.components.DaggerGeneralComponent;
import cl.test.payment.di.components.GeneralComponent;
import cl.test.payment.di.modules.GeneralModule;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import retrofit2.http.GET;
import retrofit2.http.Url;
import timber.log.Timber;

public class PaymentApp extends Application {

    public static GeneralComponent generalComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
        initTimber();
    }

    private void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    private void initDagger() {
        generalComponent = DaggerGeneralComponent.builder()
                .generalModule(new GeneralModule(this))
                .build();
    }
}
