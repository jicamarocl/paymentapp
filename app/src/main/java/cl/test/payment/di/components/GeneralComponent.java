package cl.test.payment.di.components;

import javax.inject.Singleton;

import cl.test.payment.di.modules.GeneralModule;
import cl.test.payment.views.viewmodel.PaymentViewModel;
import dagger.Component;

@Singleton
@Component(modules = {GeneralModule.class})
public interface GeneralComponent {

    void inject(PaymentViewModel paymentViewModel);

}
