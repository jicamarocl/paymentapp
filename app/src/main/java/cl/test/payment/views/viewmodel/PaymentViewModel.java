package cl.test.payment.views.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import cl.test.payment.data.PaymentRepository;
import cl.test.payment.data.model.Error;
import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.di.app.PaymentApp;
import cl.test.payment.utils.PaymentSingleObserver;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PaymentViewModel extends ViewModel implements LifecycleObserver {

    @Inject
    PaymentRepository paymentRepository;

    private MutableLiveData<PaymentMethod> selectedPaymentMethod = new MutableLiveData<>();

    private MutableLiveData<Issuer> selectedIssuer = new MutableLiveData<>();

    private MutableLiveData<Integer> paymentAmount = new MutableLiveData<>();

    private MutableLiveData<InstallmentDetail> selectedInstallment = new MutableLiveData<>();

    private MutableLiveData<List<Installment>> resultingInstallment = new MutableLiveData<>();

    private MutableLiveData<Integer> spinnerSelectedPosition = new MutableLiveData<>();

    private LiveData<List<Issuer>> issuers = Transformations.switchMap(selectedPaymentMethod,
            new Function<PaymentMethod, LiveData<List<Issuer>>>() {
                @Override
                public LiveData<List<Issuer>> apply(PaymentMethod input) {
                    return paymentRepository.getIssuers(input.getId());
                }
            });

    private LiveData<List<PaymentMethod>> paymentMethods;

    private MutableLiveData<Boolean> showLoading = new MutableLiveData<>();

    private MutableLiveData<String> errorText = new MutableLiveData<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PaymentViewModel(){
        initDagger();
    }

    private void initDagger() {
        PaymentApp.generalComponent.inject(this);
    }

    public LiveData<Boolean> getShowLoading() {
        return showLoading;
    }

    public LiveData<String> getErrorText() {
        return errorText;
    }

    public MutableLiveData<Integer> getSpinnerSelectedPosition() {
        return spinnerSelectedPosition;
    }

    public void setSpinnerSelectedPosition(Integer position) {
        spinnerSelectedPosition.setValue(position);
    }

    public MutableLiveData<List<Installment>> getInstallments() {
        return resultingInstallment;
    }

    public LiveData<List<PaymentMethod>> getPaymentMethods() {
        if (paymentMethods == null) {
            paymentMethods = paymentRepository.getPaymentMethods();
        }

        return paymentMethods;
    }

    public void getPaymentMethodsFromRemote(boolean isConnected) {
        if (isConnected) {
            showLoading.setValue(true);
            paymentRepository.getPaymentMethodsFromRemote()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<List<PaymentMethod>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(List<PaymentMethod> paymentMethods) {
                            savePaymentMethods(paymentMethods);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            showLoading.setValue(false);
                        }
                    });
        }
    }

    public void savePaymentMethods(final List<PaymentMethod> paymentMethods) {
        Completable.fromAction(new Action() {
                    @Override
                    public void run() {
                        paymentRepository.savePaymentMethods(paymentMethods);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("Success!");
                        showLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void setSelectedPaymentMethod(PaymentMethod selectedPaymentMethod) {
        this.selectedPaymentMethod.setValue(selectedPaymentMethod);
    }

    public LiveData<Integer> getAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Integer selectedPaymentAmount) {
        this.paymentAmount.setValue(selectedPaymentAmount);
    }

    public void setSelectedIssuer(Issuer issuer) {
        this.selectedIssuer.setValue(issuer);
    }

    public void setSelectedInstallment(InstallmentDetail installment) {
        this.selectedInstallment.setValue(installment);
    }

    public LiveData<List<Issuer>> getIssuers() {
        return issuers;
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    public void getIssuerFromRemote(boolean isConnected) {
        if (isConnected) {
            showLoading.setValue(true);
            paymentRepository.getIssuersFromRemote(selectedPaymentMethod.getValue().getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<List<Issuer>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(List<Issuer> issuers) {
                            saveIssuers(issuers);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            showLoading.setValue(false);
                        }
                    });
        }
    }

    private void saveIssuers(final List<Issuer> issuers) {
        Completable.fromAction(new Action() {
                    @Override
                    public void run() {
                        paymentRepository.saveIssuers(issuers);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("Success!");
                        showLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void getInstallmentFromRemote(boolean isConnected) {
        if (isConnected) {
            showLoading.setValue(true);
            paymentRepository.getInstallmentsFromRemote(selectedPaymentMethod.getValue().getId(),
                    selectedIssuer.getValue().getId(), paymentAmount.getValue().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new PaymentSingleObserver<List<Installment>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(List<Installment> installments) {
                            resultingInstallment.setValue(installments);
                            showLoading.setValue(false);
                            errorText.setValue(null);
                        }

                        @Override
                        public void onError(Error error) {
                            if (error.getThrowable() != null) {
                                error.getThrowable().printStackTrace();
                                Timber.d("Error: %s", error.getThrowable().getMessage());
                                errorText.setValue(error.getThrowable().getMessage());
                            } else {
                                Timber.d("Error: %s",
                                        error.getCause().get(0).getDescription());
                                errorText.setValue(error.getCause().get(0).getDescription());
                            }
                            showLoading.setValue(false);
                        }
                    });
        }
    }

    public MutableLiveData<Integer> getPaymentAmount() {
        return paymentAmount;
    }

    public MutableLiveData<Issuer> getSelectedIssuer() {
        return selectedIssuer;
    }

    public MutableLiveData<PaymentMethod> getSelectedPaymentMethod() {
        return selectedPaymentMethod;
    }

    public MutableLiveData<InstallmentDetail> getSelectedInstallment() {
        return selectedInstallment;
    }
}
