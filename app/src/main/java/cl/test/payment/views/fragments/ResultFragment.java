package cl.test.payment.views.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class ResultFragment extends Fragment {

    public static ResultFragment newInstance() {
        return new ResultFragment();
    }

    @BindView(R.id.issuer_text)
    TextView issuerText;

    @BindView(R.id.payment_method_text)
    TextView paymentMethodText;

    @BindView(R.id.installment_text)
    TextView installmentText;

    @BindView(R.id.installment_rate_text)
    TextView installmentRate;

    @BindView(R.id.installment_amount_text)
    TextView installmentAmount;

    @BindView(R.id.total_amount_text)
    TextView totalAmount;

    @BindView(R.id.amount_text)
    TextView amountText;

    private PaymentViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result,
                container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPaymentAmount();
        initSelectedPaymentMethod();
        initSelectedIssuer();
        initSelectedInstallment();
    }

    private void initSelectedIssuer() {
        viewModel.getSelectedIssuer().observe(this, new Observer<Issuer>() {
            @Override
            public void onChanged(@Nullable Issuer issuer) {
                if (issuer != null) {
                    issuerText.setText(issuer.getName());
                }
            }
        });
    }

    private void initPaymentAmount() {
        viewModel.getPaymentAmount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    amountText.setText(integer.toString());
                }
            }
        });
    }

    private void initSelectedPaymentMethod() {
        viewModel.getSelectedPaymentMethod().observe(this, new Observer<PaymentMethod>() {
            @Override
            public void onChanged(@Nullable PaymentMethod paymentMethod) {
                if (paymentMethod != null) {
                    paymentMethodText.setText(paymentMethod.getName());
                }
            }
        });
    }

    private void initSelectedInstallment() {
        viewModel.getSelectedInstallment().observe(this, new Observer<InstallmentDetail>() {
            @Override
            public void onChanged(@Nullable InstallmentDetail installmentDetail) {
                if (installmentDetail != null) {
                    installmentText.setText(String.format(Locale.getDefault(), "%d",
                            installmentDetail.getInstallments()));
                    installmentRate.setText(String.format(Locale.getDefault(), "%.2f",
                            installmentDetail.getInstallmentRate()));
                    installmentAmount.setText(String.format(Locale.getDefault(), "%.2f",
                            installmentDetail.getInstallmentAmount()));
                    totalAmount.setText(String.format(Locale.getDefault(), "%.2f",
                            installmentDetail.getTotalAmount()));
                }
            }
        });
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(PaymentViewModel.class);
    }
}
