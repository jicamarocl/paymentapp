package cl.test.payment.views.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.views.adapter.PaymentMethodAdapter;
import cl.test.payment.views.interfaces.OnItemClickListener;
import cl.test.payment.views.viewmodel.PaymentViewModel;
import timber.log.Timber;

public class PaymentMethodFragment extends Fragment {

    public static PaymentMethodFragment newInstance() {
        return new PaymentMethodFragment();
    }

    private PaymentViewModel viewModel;

    private PaymentMethodAdapter adapter;

    @BindView(R.id.payment_method_list)
    RecyclerView paymentMethodList;

    @BindView(R.id.loader)
    ProgressBar loading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_method,
                container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(PaymentViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLoading();
        initAdapter();
        initRecycler();
        initPaymentMethods();
        initFromRemote();
    }

    private void initLoading() {
        viewModel.getShowLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    loading.setVisibility(View.VISIBLE);
                    paymentMethodList.setVisibility(View.GONE);
                } else {
                    loading.setVisibility(View.GONE);
                    paymentMethodList.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initRecycler() {
        paymentMethodList.setLayoutManager(new LinearLayoutManager(requireActivity(),
                LinearLayoutManager.VERTICAL, false));
        paymentMethodList.setHasFixedSize(true);
        paymentMethodList.setAdapter(adapter);
    }

    private void initAdapter() {
        adapter = new PaymentMethodAdapter(viewModel, new OnItemClickListener<PaymentMethod>() {
            @Override
            public void onItemClickListener(int position, PaymentMethod item) {
                goToIssuers();
            }
        });
    }

    private void initFromRemote() {
        viewModel.getPaymentMethodsFromRemote(true);
    }

    private void goToIssuers() {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, IssuerFragment.newInstance(), "issuers")
                .addToBackStack("issuers")
                .commit();
    }

    private void initPaymentMethods() {
        viewModel.getPaymentMethods().observe(this, new Observer<List<PaymentMethod>>() {
            @Override
            public void onChanged(@Nullable List<PaymentMethod> paymentMethods) {
                adapter.addAll(paymentMethods);
            }
        });
    }
}
