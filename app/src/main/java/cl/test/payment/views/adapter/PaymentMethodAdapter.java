package cl.test.payment.views.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import cl.test.payment.R;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.views.interfaces.OnItemClickListener;
import cl.test.payment.views.viewholder.PaymentMethodViewHolder;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodViewHolder> {

    private LinkedList<PaymentMethod> paymentMethods = new LinkedList<>();

    private PaymentViewModel paymentViewModel;

    private OnItemClickListener<PaymentMethod> onItemClickListener;

    public PaymentMethodAdapter(PaymentViewModel paymentViewModel,
                                OnItemClickListener<PaymentMethod> onItemClickListener) {
        this.paymentViewModel = paymentViewModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public PaymentMethodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_method_item, parent, false);
        return new PaymentMethodViewHolder(paymentViewModel, view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodViewHolder holder, int position) {
        holder.init(paymentMethods.get(position));
    }

    @Override
    public int getItemCount() {
        return paymentMethods.size();
    }

    public void addAll(List<PaymentMethod> items) {
        paymentMethods.addAll(items);
        notifyDataSetChanged();
    }
}
