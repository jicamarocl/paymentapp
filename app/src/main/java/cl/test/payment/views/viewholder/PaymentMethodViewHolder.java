package cl.test.payment.views.viewholder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.utils.GlideApp;
import cl.test.payment.views.interfaces.OnItemClickListener;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class PaymentMethodViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.payment_method_name)
    TextView paymentMethodNameLabel;

    @BindView(R.id.payment_method_badge)
    ImageView paymentMethodBadge;

    @BindView(R.id.payment_method_card)
    CardView paymentMethodCard;

    private PaymentViewModel paymentViewModel;

    private OnItemClickListener<PaymentMethod> onItemClickListener;

    public PaymentMethodViewHolder(PaymentViewModel paymentViewModel, View itemView, OnItemClickListener<PaymentMethod> onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.paymentViewModel = paymentViewModel;
        this.onItemClickListener = onItemClickListener;
    }

    public void init(final PaymentMethod paymentMethod) {
        paymentMethodNameLabel.setText(paymentMethod.getName());
        GlideApp.with(itemView).asBitmap().load(paymentMethod.getSecureThumbnail()).centerInside()
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model,
                                                   Target<Bitmap> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        Palette.Builder builder = new Palette.Builder(resource);
                        builder.generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(@NonNull Palette palette) {
                                int rgb = palette.getLightMutedColor(ContextCompat
                                        .getColor(itemView.getContext(), android.R.color.white));
                                paymentMethodCard.setCardBackgroundColor(rgb);
                            }
                        });

                        return false;
                    }
                })
                .into(paymentMethodBadge);

        paymentMethodCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentViewModel.setSelectedPaymentMethod(paymentMethod);
                onItemClickListener.onItemClickListener(getAdapterPosition(), paymentMethod);
            }
        });
    }
}
