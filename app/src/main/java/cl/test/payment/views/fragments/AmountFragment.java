package cl.test.payment.views.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.utils.Constants;
import cl.test.payment.views.adapter.PaymentMethodAdapter;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class AmountFragment extends Fragment {

    public static AmountFragment newInstance() {
        return new AmountFragment();
    }

    private PaymentViewModel viewModel;

    @BindView(R.id.amount_text)
    TextInputEditText amountText;

    @BindView(R.id.amount_next)
    Button nextButton;

    @BindView(R.id.amount_text_layout)
    TextInputLayout amountLayout;

    @BindView(R.id.result_installment)
    ViewGroup resultInstallment;

    private boolean isShowingResult;

    private TextWatcher listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            resultInstallment.setVisibility(View.GONE);
            isShowingResult = false;
        }

        @Override
        public void afterTextChanged(final Editable s) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (!s.toString().isEmpty()) {
                                Integer integer = Integer.parseInt(s.toString());
                                viewModel.setPaymentAmount(integer);
                            }
                        }
                    });
                }
            }, 1000);
        }
    };

    private Timer timer = new Timer();

    private ResultFragment resultFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();

        if (savedInstanceState != null) {
            isShowingResult = savedInstanceState.getBoolean(Constants.IS_SHOWING_RESULT,
                    isShowingResult);
        }
    }

    private void initButton() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!amountText.getText().toString().isEmpty()) {
                    boolean isGreaterThanZero = Integer.parseInt(amountText.getText()
                            .toString()) > 0;

                    if (!isGreaterThanZero) {
                        amountLayout.setError(requireContext().getResources()
                                .getString(R.string.error_amount_zero));
                        return;
                    }
                    amountLayout.setError(null);
                    viewModel.getSpinnerSelectedPosition().setValue(0);
                    goToNext();
                } else {
                    amountLayout.setError(requireContext()
                            .getResources().getString(R.string.error_amount_empty));
                }
            }
        });
    }

    private void goToNext() {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, PaymentMethodFragment.newInstance())
                .addToBackStack("payment_methods")
                .commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_amount,
                container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Constants.IS_SHOWING_RESULT, isShowingResult);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initButton();
        initText();
        initSelectedInstallment();
    }

    private void initSelectedInstallment() {
        viewModel.getSelectedInstallment().observe(this, new Observer<InstallmentDetail>() {
            @Override
            public void onChanged(@Nullable InstallmentDetail installmentDetail) {
                if (installmentDetail != null) {
                    resultFragment = ResultFragment.newInstance();
                    getChildFragmentManager().beginTransaction()
                            .add(R.id.result_installment, resultFragment).commit();
                    resultInstallment.setVisibility(View.VISIBLE);
                    isShowingResult = true;
                } else {
                    resultInstallment.setVisibility(View.GONE);
                    isShowingResult = false;
                }
            }
        });
    }

    private void initText() {
        amountText.requestFocus();
        amountText.addTextChangedListener(listener);
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(PaymentViewModel.class);
    }
}
