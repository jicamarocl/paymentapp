package cl.test.payment.views.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.utils.Constants;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class InstallmentFragment extends Fragment {

    public static InstallmentFragment newInstance() {
        return new InstallmentFragment();
    }

    private PaymentViewModel viewModel;

    @BindView(R.id.installment_quantity)
    Spinner spinner;

    @BindView(R.id.installment_next)
    Button nextButton;

    @BindView(R.id.installment_card)
    CardView installmentCard;

    @BindView(R.id.loader)
    ProgressBar loading;

    @BindView(R.id.error_card)
    CardView errorCard;

    @BindView(R.id.error_text)
    TextView errorText;

    private ArrayAdapter<InstallmentDetail> arrayAdapter;

    private int selectedPosition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();

        if (savedInstanceState != null) {
            selectedPosition = savedInstanceState.getInt(Constants.SPINNER_SELECTED_POSITION
                    , 0);
        } else {
            selectedPosition = 0;
        }
    }

    private void initButton() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAmountWithResult();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_installment,
                container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLoading();
        initError();
        initButton();
        initInstallment();
        initSelectedInstallment();
    }

    private void initSpinnerSelectedPosition() {
        if (viewModel.getSpinnerSelectedPosition().getValue() != null) {
            selectedPosition = viewModel.getSpinnerSelectedPosition().getValue();
            spinner.setSelection(selectedPosition);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.SPINNER_SELECTED_POSITION, selectedPosition);
    }

    private void initError() {
        viewModel.getErrorText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s != null && !s.isEmpty()) {
                    errorText.setText(s);
                    errorCard.setVisibility(View.VISIBLE);
                    installmentCard.setVisibility(View.GONE);
                } else {
                    errorCard.setVisibility(View.GONE);
                    installmentCard.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initLoading() {
        viewModel.getShowLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    loading.setVisibility(View.VISIBLE);
                    errorCard.setVisibility(View.GONE);
                    installmentCard.setVisibility(View.GONE);
                } else {
                    loading.setVisibility(View.GONE);
                    String s = viewModel.getErrorText().getValue();
                    if (s != null && !s.isEmpty()) {
                        errorCard.setVisibility(View.VISIBLE);
                    } else {
                        installmentCard.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void initSelectedInstallment() {
        viewModel.getSelectedInstallment().observe(this, new Observer<InstallmentDetail>() {
            @Override
            public void onChanged(@Nullable InstallmentDetail installmentDetail) {
                if (installmentDetail != null) {
                    getChildFragmentManager().beginTransaction()
                            .add(R.id.result_installment, ResultFragment.newInstance()).commit();
                }
            }
        });
    }

    private void initArrayAdapter(List<Installment> installments) {
        arrayAdapter = new ArrayAdapter<>(requireActivity(),
                R.layout.installment_item, R.id.installment_option);
        for (Installment installment : installments) {
            arrayAdapter.addAll(installment.getInstallmentDetail());
        }
    }

    private void initInstallment() {
        viewModel.getInstallments().observe(this, new Observer<List<Installment>>() {
            @Override
            public void onChanged(@Nullable List<Installment> installments) {
                initArrayAdapter(installments);
                initSpinner(installments);
            }
        });

        viewModel.getInstallmentFromRemote(true);
    }

    private void initSpinner(final List<Installment> installments) {
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (installments.get(0) != null) {
                    viewModel.setSpinnerSelectedPosition(position);
                    viewModel.setSelectedInstallment(installments.get(0)
                            .getInstallmentDetail().get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        initSpinnerSelectedPosition();
    }

    private void goToAmountWithResult() {
        requireActivity().getSupportFragmentManager().popBackStack(null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(PaymentViewModel.class);
    }
}
