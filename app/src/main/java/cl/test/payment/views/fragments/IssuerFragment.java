package cl.test.payment.views.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.views.adapter.IssuerAdapter;
import cl.test.payment.views.adapter.PaymentMethodAdapter;
import cl.test.payment.views.interfaces.OnItemClickListener;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class IssuerFragment extends Fragment {

    public static IssuerFragment newInstance() {
        return new IssuerFragment();
    }

    private PaymentViewModel viewModel;

    @BindView(R.id.issuer_list)
    RecyclerView issuerList;

    @BindView(R.id.loader)
    ProgressBar loading;

    private IssuerAdapter adapter;

    private void initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(PaymentViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issuer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void initIssuers() {
        viewModel.getIssuers().observe(this, new Observer<List<Issuer>>() {
            @Override
            public void onChanged(@Nullable List<Issuer> issuers) {
                if (issuers != null && !issuers.isEmpty()) {
                    adapter.addAll(issuers);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLoading();
        initAdapter();
        initRecycler();
        initIssuers();
        initFromRemote();
    }

    private void initLoading() {
        viewModel.getShowLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    loading.setVisibility(View.VISIBLE);
                    issuerList.setVisibility(View.GONE);
                } else {
                    loading.setVisibility(View.GONE);
                    issuerList.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initRecycler() {
        issuerList.setLayoutManager(new LinearLayoutManager(requireActivity(),
                LinearLayoutManager.VERTICAL, false));
        issuerList.setHasFixedSize(true);
        issuerList.setAdapter(adapter);
    }

    private void goToInstallments() {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, InstallmentFragment.newInstance())
                .addToBackStack("installments")
                .commit();
    }

    private void initAdapter() {
        adapter = new IssuerAdapter(viewModel, new OnItemClickListener<Issuer>() {
            @Override
            public void onItemClickListener(int position, Issuer item) {
                goToInstallments();
            }
        });
    }

    private void initFromRemote() {
        viewModel.getIssuerFromRemote(true);
    }
}
