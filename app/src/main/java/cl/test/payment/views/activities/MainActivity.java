package cl.test.payment.views.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.test.payment.R;
import cl.test.payment.views.fragments.AmountFragment;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class MainActivity extends AppCompatActivity {

    private PaymentViewModel viewModel;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initViewModel();
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            return;
        }

        initFragment();
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
    }

    private void initFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, AmountFragment.newInstance())
                .commit();
    }
}
