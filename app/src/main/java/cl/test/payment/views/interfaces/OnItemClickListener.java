package cl.test.payment.views.interfaces;

public interface OnItemClickListener<T> {

    void onItemClickListener(int position, T item);

}
