package cl.test.payment.views.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import cl.test.payment.R;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.views.interfaces.OnItemClickListener;
import cl.test.payment.views.viewholder.IssuerViewHolder;
import cl.test.payment.views.viewholder.PaymentMethodViewHolder;
import cl.test.payment.views.viewmodel.PaymentViewModel;

public class IssuerAdapter extends RecyclerView.Adapter<IssuerViewHolder> {

    private LinkedList<Issuer> issuers = new LinkedList<>();

    private PaymentViewModel paymentViewModel;

    private OnItemClickListener<Issuer> onItemClickListener;

    public IssuerAdapter(PaymentViewModel paymentViewModel,
                         OnItemClickListener<Issuer> onItemClickListener) {
        this.paymentViewModel = paymentViewModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public IssuerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.issuer_item, parent, false);
        return new IssuerViewHolder(paymentViewModel, view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull IssuerViewHolder holder, int position) {
        holder.init(issuers.get(position));
    }

    @Override
    public int getItemCount() {
        return issuers.size();
    }

    public void addAll(List<Issuer> items) {
        issuers.addAll(items);
        notifyDataSetChanged();
    }
}
