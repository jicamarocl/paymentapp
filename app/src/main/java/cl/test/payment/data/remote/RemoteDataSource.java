package cl.test.payment.data.remote;

import java.util.List;

import javax.inject.Inject;

import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.utils.Constants;
import io.reactivex.Single;

public class RemoteDataSource {

    private PaymentService paymentService;

    @Inject
    public RemoteDataSource(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    public Single<List<PaymentMethod>> getPaymentMethods() {
        return paymentService.getPaymentMethods(Constants.PUBLIC_KEY);
    }

    public Single<List<Issuer>> getIssuers(String paymentMethodId) {
        return paymentService.getIssuers(Constants.PUBLIC_KEY, paymentMethodId);
    }

    public Single<List<Installment>> getInstallments(String paymentMethodId, String issuerId,
                                                     String amount) {
        return paymentService.getInstallments(Constants.PUBLIC_KEY, paymentMethodId,
                issuerId, amount);
    }
}
