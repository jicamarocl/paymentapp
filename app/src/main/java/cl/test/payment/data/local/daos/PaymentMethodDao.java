package cl.test.payment.data.local.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import cl.test.payment.data.model.PaymentMethod;

@Dao
public interface PaymentMethodDao {

    @Query("SELECT * FROM payment_method")
    LiveData<List<PaymentMethod>> getAll();

    @Query("SELECT COUNT(*) FROM payment_method")
    int getPaymentMethodCount();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PaymentMethod> paymentMethods);
}
