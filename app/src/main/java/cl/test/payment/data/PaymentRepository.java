package cl.test.payment.data;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import cl.test.payment.data.local.LocalDataSource;
import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import cl.test.payment.data.remote.RemoteDataSource;
import io.reactivex.Single;

@Singleton
public class PaymentRepository {

    private LocalDataSource localDataSource;
    private RemoteDataSource remoteDataSource;

    @Inject
    public PaymentRepository(LocalDataSource localDataSource, RemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public LiveData<List<Issuer>> getIssuers(String paymentMethodId) {
        return localDataSource.getIssuers(paymentMethodId);
    }

    public LiveData<List<PaymentMethod>> getPaymentMethods() {
        return localDataSource.getPaymentMethods();
    }

    public void savePaymentMethods(List<PaymentMethod> paymentMethods) {
        int localPaymentMethods = localDataSource.getPaymentMethodCount();
        if (localPaymentMethods == 0) {
            localDataSource.savePaymentMethods(paymentMethods);
        }
    }

    public void saveIssuers(List<Issuer> issuers) {
        int localIssuerCount = localDataSource.getIssuerCount();
        if (localIssuerCount == 0) {
            localDataSource.saveIssuers(issuers);
        }
    }

    public Single<List<PaymentMethod>> getPaymentMethodsFromRemote() {
        return remoteDataSource.getPaymentMethods();
    }

    public Single<List<Issuer>> getIssuersFromRemote(String paymentMethodId) {
        return remoteDataSource.getIssuers(paymentMethodId);
    }


    public Single<List<Installment>> getInstallmentsFromRemote(String paymentMethodId,
                                                               String issuerId,
                                                               String amount) {
        return remoteDataSource.getInstallments(paymentMethodId, issuerId, amount);
    }
}
