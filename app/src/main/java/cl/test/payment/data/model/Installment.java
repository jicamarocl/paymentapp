package cl.test.payment.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "installment")
public class Installment {

    @PrimaryKey
    @ColumnInfo(name = "objectId")
    private Long objectId;
    @ColumnInfo(name = "payment_method_id")
    @SerializedName("payment_method_id")
    private String paymentMethodId;
    @ColumnInfo(name = "payment_type_id")
    @SerializedName("payment_type_id")
    private String paymentTypeId;
    @ColumnInfo(name = "issuer")
    @SerializedName("issuer")
    private Issuer issuer;
    @ColumnInfo(name = "payer_costs")
    @SerializedName("payer_costs")
    private List<InstallmentDetail> installmentDetail;

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public List<InstallmentDetail> getInstallmentDetail() {
        return installmentDetail;
    }

    public void setInstallmentDetail(List<InstallmentDetail> installmentDetail) {
        this.installmentDetail = installmentDetail;
    }
}
