package cl.test.payment.data.local;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import cl.test.payment.data.local.daos.IssuerDao;
import cl.test.payment.data.local.daos.PaymentMethodDao;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;

public class LocalDataSource {

    private final PaymentMethodDao paymentMethodDao;
    private final IssuerDao issuerDao;

    @Inject
    public LocalDataSource(PaymentMethodDao paymentMethodDao, IssuerDao issuerDao) {
        this.issuerDao = issuerDao;
        this.paymentMethodDao = paymentMethodDao;
    }

    public void savePaymentMethods(List<PaymentMethod> paymentMethods) {
        paymentMethodDao.insertAll(paymentMethods);
    }

    public void saveIssuers(List<Issuer> issuers) {
        issuerDao.insertAll(issuers);
    }

    public LiveData<List<Issuer>> getIssuers(String paymentMethodId) {
        return issuerDao.getAll();
    }

    public LiveData<List<PaymentMethod>> getPaymentMethods() {
        return paymentMethodDao.getAll();
    }

    public int getPaymentMethodCount() {
        return paymentMethodDao.getPaymentMethodCount();
    }

    public int getIssuerCount() {
        return issuerDao.getIssuerCount();
    }
}
