package cl.test.payment.data.local.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import cl.test.payment.data.model.Issuer;

@Dao
public interface IssuerDao {

    @Query("SELECT * FROM issuer")
    LiveData<List<Issuer>> getAll();

    @Query("SELECT COUNT(*) FROM issuer")
    int getIssuerCount();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Issuer> issuers);


}
