package cl.test.payment.data.model;

import java.util.List;

public class Error {

    private String message;
    private String error;
    private String status;
    private List<ErrorCause> cause;

    private Throwable throwable;

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

    public String getStatus() {
        return status;
    }

    public List<ErrorCause> getCause() {
        return cause;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
