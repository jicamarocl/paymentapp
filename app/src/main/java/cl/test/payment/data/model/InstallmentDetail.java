package cl.test.payment.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "installment_detail")
public class InstallmentDetail {

    @PrimaryKey
    @ColumnInfo(name = "objectId")
    private Long objectId;
    @ColumnInfo(name = "installments")
    private int installments;
    @ColumnInfo(name = "installment_rate")
    @SerializedName("installment_rate")
    private float installmentRate;
    @ColumnInfo(name = "labels")
    private List<String> labels;
    @ColumnInfo(name = "min_allowed_amount")
    @SerializedName("min_allowed_amount")
    private int minAllowedAmount;
    @ColumnInfo(name = "max_allowed_amount")
    @SerializedName("max_allowed_amount")
    private int maxAllowedAmount;
    @ColumnInfo(name = "recommended_message")
    @SerializedName("recommended_message")
    private String recommendedMessage;
    @ColumnInfo(name = "installment_amount")
    @SerializedName("installment_amount")
    private float installmentAmount;
    @ColumnInfo(name = "total_amount")
    @SerializedName("total_amount")
    private float totalAmount;

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public float getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(float installmentRate) {
        this.installmentRate = installmentRate;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public int getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(int minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public int getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(int maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public float getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(float installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return recommendedMessage;
    }
}
