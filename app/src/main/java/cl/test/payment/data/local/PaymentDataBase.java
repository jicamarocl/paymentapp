package cl.test.payment.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import cl.test.payment.data.local.daos.IssuerDao;
import cl.test.payment.data.local.daos.PaymentMethodDao;
import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.InstallmentDetail;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;

@Database(entities = {Issuer.class, PaymentMethod.class, Installment.class, InstallmentDetail.class}, version = 1)
@TypeConverters(cl.test.payment.utils.TypeConverters.class)
public abstract class PaymentDataBase extends RoomDatabase {

    private static volatile PaymentDataBase INSTANCE;

    public abstract PaymentMethodDao paymentMethodDao();
    public abstract IssuerDao issuerDao();

    public static PaymentDataBase buildPersistentDataSource(Context context) {
        if (INSTANCE == null) {
            synchronized (PaymentDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PaymentDataBase.class, "payment_db")
                            .build();
                }
            }
        }

        return INSTANCE;
    }
}
