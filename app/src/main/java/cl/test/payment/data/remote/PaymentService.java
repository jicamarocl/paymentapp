package cl.test.payment.data.remote;

import java.util.List;

import cl.test.payment.data.model.Installment;
import cl.test.payment.data.model.Issuer;
import cl.test.payment.data.model.PaymentMethod;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PaymentService {

    @GET("/v1/payment_methods")
    Single<List<PaymentMethod>> getPaymentMethods(@Query("public_key") String publicKey);

    @GET("/v1/payment_methods/card_issuers")
    Single<List<Issuer>> getIssuers(@Query("public_key") String publicKey,
                                    @Query("payment_method_id") String paymentMethodId);

    @GET("/v1/payment_methods/installments")
    Single<List<Installment>> getInstallments(@Query("public_key") String publicKey,
                                              @Query("payment_method_id") String paymentMethodId,
                                              @Query("issuer.id") String issuerId,
                                              @Query("amount") String amount);
}
