## PaymentApp

Este repositorio contiene la aplicación realizada para la prueba de MercadoLibre.

Esta fue realizada en el lenguaje Java para la plataforma Android. Otras consideraciones del proyecto son:

* Se utilizó Android Architecture Components como arquitectura, esta es similar a MVVM.
* Se utilizó la biblioteca AppCompat.
* Se utilizó LiveData + Room para el guardado local de data y su observación en UI.
* NO se utilizo Data Binding.
* Para la conexión remota se utilizó Retrofit + OkHTTP + RxJava2 .
* Para la inyección de dependencias se utilizó Dagger.
* Para las imágenes se utilizó Glide.
* Se utiliza Single Activity +  Fragmentos sin el uso de Navegation incluído en Android 28 (P).

#### Estructura del proyecto

La estructura del proyecto es la siguiente:

##### data

* local
* model
* remote

##### di

* app
* components
* modules
* utils

##### utils

##### views

* activities
* adapter
* fragments
* interfaces
* viewholder
* viewmodel

